# Build pipelines for custom containers

 * Keycloak-operator for ARM64, which also creates CRDs and operator deployment:
   - `registry.gitlab.com/agravgaard/custom-containers/keycloak-operator:latest`
   - [keycloakrealmimports.k8s.keycloak.org-v1.yml](https://agravgaard.gitlab.io/custom-containers/keycloakrealmimports.k8s.keycloak.org-v1.yml)
   - [keycloaks.k8s.keycloak.org-v1.yml](https://agravgaard.gitlab.io/custom-containers/keycloaks.k8s.keycloak.org-v1.yml)
   - [kubernetes.yml](https://agravgaard.gitlab.io/custom-containers/kubernetes.yml)
 * Talos for Turing Pi RK1 with DRBD:
   - `registry.gitlab.com/agravgaard/custom-containers/talos-installer:v1.7.4-rk3588`
 * Custom Open Policy Agent (OPA) images:
   - `registry.gitlab.com/agravgaard/custom-containers/opa-envoy-plugin`
   - `registry.gitlab.com/agravgaard/custom-containers/opa-admission-controller`
