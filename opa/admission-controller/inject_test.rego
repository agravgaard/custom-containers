package istio_test

import rego.v1

import data.istio.inject

sample_input := {"request": {"uid": "test"}}

test_inject if {
	inject.response.uid == "test" with input as sample_input
	json.unmarshal(base64.decode(inject.response.patch))[0].op == "add" with input as sample_input
	json.unmarshal(base64.decode(inject.response.patch))[0].value.name == "opa-istio" with input as sample_input
}
