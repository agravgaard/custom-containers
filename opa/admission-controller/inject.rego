package istio

import rego.v1

uid := input.request.uid

inject := {
	"apiVersion": "admission.k8s.io/v1",
	"kind": "AdmissionReview",
	"response": {
		"allowed": true,
		"uid": uid,
		"patchType": "JSONPatch",
		"patch": base64.encode(json.marshal(patch)),
	},
}

patch := [{
	"op": "add",
	"path": "/spec/containers/-",
	"value": opa_container,
}]

opa_container := {
	"image": "registry.gitlab.com/agravgaard/custom-containers/opa-envoy-plugin:latest",
	"name": "opa-istio",
	"args": [
		"run",
		"--server",
		"--config-file=/config/config.yaml",
		"--addr=localhost:8181",
		"--diagnostic-addr=0.0.0.0:8282",
		"/policies/policy.rego",
	],
	"readinessProbe": {"httpGet": {
		"path": "/health?plugins",
		"port": 8282,
	}},
	"livenessProbe": {"httpGet": {
		"path": "/health?plugins",
		"port": 8282,
	}},
}
