## Notes:

`bundle.tar.gz` is an empty file created by:
```bash
touch bundle.tar.gz
chmod a+w bundle.tar.gz
```
We copy it into the image as a workaround for the nonroot user not having permission to create a file.
