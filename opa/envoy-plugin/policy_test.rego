package istio.authz_test

import rego.v1

import data.istio.authz.allow

sample_input_health := {
	"attributes": {
		"destination": {"address": {"Address": {"SocketAddress": {"PortSpecifier": {"PortValue": 80}, "address": "192.168.208.7"}}}},
		"metadata_context": {},
		"request": {
			"http": {
				"headers": {
					":authority": "localhost:8080",
					":method": "GET",
					":path": "/health", "accept": "*/*", "accept-encoding": "*", "accept-language": "en-US,en;q=0.9", "cache-control": "no-cache", "dnt": "1", "pragma": "no-cache",
					"referer": "http://localhost:8080/",
					"sec-fetch-dest": "image", "sec-fetch-mode": "no-cors", "sec-fetch-site": "same-origin",
					"user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36",
					"x-forwarded-proto": "http", "x-request-id": "6ee22bb4-b0cc-4af0-835f-38f6b9f7286b",
				},
				"host": "localhost:8080",
				"id": "10400343980969087870",
				"method": "GET",
				"path": "/health",
				"protocol": "HTTP/1.1",
			},
			"time": {"nanos": 979189000, "seconds": 1597706419},
		},
		"source": {"address": {"Address": {"SocketAddress": {"PortSpecifier": {"PortValue": 56258}, "address": "192.168.208.1"}}}},
	},
	"parsed_body": null,
	"parsed_path": ["health"],
	"parsed_query": {},
}

test_get_health_allowed if {
	allow with input as sample_input_health
}

sample_input_unshort := {
	"attributes": {
		"destination": {"address": {"Address": {"SocketAddress": {"PortSpecifier": {"PortValue": 80}, "address": "192.168.208.7"}}}},
		"metadata_context": {},
		"request": {
			"http": {
				"headers": {
					":authority": "localhost:8080",
					":method": "GET",
					":path": "/", "accept": "*/*", "accept-encoding": "*", "accept-language": "en-US,en;q=0.9", "cache-control": "no-cache", "dnt": "1", "pragma": "no-cache",
					"referer": "http://localhost:8080/",
					"sec-fetch-dest": "image", "sec-fetch-mode": "no-cors", "sec-fetch-site": "same-origin",
					"user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36",
					"x-forwarded-proto": "http", "x-request-id": "6ee22bb4-b0cc-4af0-835f-38f6b9f7286b",
				},
				"host": "unshort.cyberbunny.cyou", "id": "10400343980969087870", "method": "GET", "path": "/", "protocol": "HTTP/1.1",
			},
			"time": {"nanos": 979189000, "seconds": 1597706419},
		},
		"source": {"address": {"Address": {"SocketAddress": {"PortSpecifier": {"PortValue": 56258}, "address": "192.168.208.1"}}}},
	},
	"parsed_body": null,
	"parsed_path": [],
	"parsed_query": {},
}

test_get_allowed_when_host_is_unshort if {
	allow with input as sample_input_unshort
}
