package istio.authz

import rego.v1

http_request := input.attributes.request.http
parsed_path := input.parsed_path

allow if {
	parsed_path[0] == "health"
	http_request.method == "GET"
}

allow if {
	print(http_request)
	http_request.method == "GET"
}

# OIDC
#issuers := {"https://auth.cyberbunny.cyou"}
#
#metadata_discovery(issuer) := http.send({
#	"url": concat("", [issuers[issuer], "/.well-known/openid-configuration"]),
#	"method": "GET",
#	"force_cache": true,
#	"force_cache_duration_seconds": 86400,
#}).body
#
#jwt_unverified := io.jwt.decode(input.token)
#
#jwt_header := jwt_unverified[0]
#
#claims := jwt_unverified[1]
#
#metadata := metadata_discovery(claims.iss)
#
#jwks_endpoint := metadata.jwks_uri
#
#token_endpoint := metadata.token_endpoint
#
#jwks_request(url) := http.send({
#	"url": url,
#	"method": "GET",
#	"force_cache": true,
#	"force_cache_duration_seconds": 3600, # Cache response for an hour
#})
#
## Use the key ID (kid) from the token as a cache key - if a new kid is encountered
## we obtain a fresh JWKS object as the keys have likely been rotated.
#jwks_url := concat("?", [
#	jwks_endpoint,
#	urlquery.encode_object({"kid": jwt_header.kid}),
#])
#
#jwks := jwks_request(jwks_url).raw_body
#
#jwt_verified := jwt_unverified if {
#	io.jwt.verify_rs256(input.token, jwks)
#	[valid, header, payload] := io.jwt.decode_verify(input.token, {
#		"cert": jwks,
#		"iss": issuers[issuer],
#	})
#	print(valid)
#	print(header)
#	print(payload)
#	valid
#}
#
#claims_verified := jwt_verified[1]
#
#allow if {
#	jwt_verified
#}
