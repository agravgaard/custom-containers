#!/bin/sh

set -eu

git clone --branch release-1.7 https://github.com/nberlee/extensions.git

cd extensions || exit 1

sed -i '/TARGETS\s*+=\s*rk3588/a TARGETS += drbd' Makefile

make docker-drbd PLATFORM=linux/arm64 TARGET_ARGS="--load -t local/drbd"
