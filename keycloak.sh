#!/bin/bash

git clone --branch 23.0.4 https://github.com/keycloak/keycloak.git

cd keycloak || exit 1

./mvnw install --no-transfer-progress -f ./pom.xml -pl common -am

./mvnw -f ./operator/pom.xml quarkus:add-extension -Dextensions='container-image-jib'

./mvnw clean package --no-transfer-progress -f ./operator/pom.xml -Doperator \
            -Dquarkus.container-image.build=true \
            -Dquarkus.container-image.builder=jib \
            -Dquarkus.container-image.registry="${CI_REGISTRY}" \
            -Dquarkus.container-image.username="${CI_REGISTRY_USER}" \
            -Dquarkus.container-image.password="${CI_REGISTRY_PASSWORD}" \
            -Dquarkus.container-image.name=keycloak-operator \
            -Dquarkus.container-image.tag="${IMAGE_TAG}" \
            -Dquarkus.container-image.group="agravgaard/custom-containers" \
            -Dquarkus.jib.platforms="linux/amd64,linux/arm64/v8" \
            -Dquarkus.container-image.push=true
