#!/usr/bin/env sh

## Modified version of install script from https://github.com/Pelochus/ezrknpu

message_print() {
  echo "## $1"
}

message_print "Installing apt dependencies..."

apt update

apt install -y \
  build-essential \
  cmake \
  git \
  git-lfs \
  libgl1 \
  libglib2.0-dev \
  libglx-mesa0 \
  libhdf5-dev \
  libprotobuf-dev \
  libsm6 \
  libxslt1-dev \
  python-is-python3 \
  python3-dev \
  python3-pip \
  zlib1g-dev

message_print "Cloning main repo with submodules..."

git clone --recurse-submodules -j2 https://github.com/Pelochus/ezrknpu
cd ezrknpu || exit 1

message_print "Updating submodules..."
git submodule update --recursive --remote

message_print "Installing RKNN LLM with install.sh script..."
cd ezrknn-llm || exit 1
bash install.sh

message_print "Installing RKNN Toolkit 2 with install.sh script..."
cd ../ezrknn-toolkit2 || exit 1
bash install.sh

message_print "Everything done!"
